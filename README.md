# Nara

Nara is a fork of the popular [MusicBot](https://github.com/just-some-bots/MusicBot/) project. Nara attempts to provide a more consistent, streamlined user experience with less complex behind-the-scenes code.  

As of now, the main differences to the upstream project are:
- Cleaner embed appearance
- Prettified & more consistent now playing messages
- Optional auto-disconnect from empty channels
- Customized default English translation
- Fix for asynchronous enqueue / clear interference
- Minor translation fixes
- Code cleanup & refactoring

![Main](https://i.imgur.com/FWcHtcS.png)

## Setup
Setting up the MusicBot is relatively painless - just follow one of the [guides](https://just-some-bots.github.io/MusicBot/). After that, configure the bot to ensure its connection to Discord.

The main configuration file is `config/options.ini`, but it is not included by default. Simply make a copy of `example_options.ini` and rename it to `options.ini`. See `example_options.ini` for more information about configurations.

### Commands

There are many commands that can be used with the bot. Most notably, the `play <url>` command (preceded by your command prefix) will download, process, and play a song from YouTube or a similar site. A full list of commands is available [here](https://just-some-bots.github.io/MusicBot/using/commands/ "Commands").
